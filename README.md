# (cons 'scheme 'sets)

This is an implementation of sets with orderable elements for R7RS Scheme, it's based on the code from the paper *Implementing Sets Efficiently in a Functional Language* by Stephen Adams.

This library exports procedures for creating sets, accessing it's elements and higher-order functions to process functionaly a set.

Most of the procedures accept one argument named *lt* (for less than), if one wishes to specialize the set data structure, this procedure is the only change that has to be wrapped.

## set type and constructors
- `<oset>` Variant type, an ordered set can be empty or be the node of an ordered binary tree
- `oset? : obj -> bool` Ordered set prediacte
- `oset-empty : () -> <oset>` Empty set constructor
- `oset : proc x obj x ... -> <oset>` Takes a *less-than* predicate and zero or more objects and constructs a set

## set queries
- `oset-size : <oset> -> integer` Returns the set cardinality
- `oset-member? : obj x <oset> x proc -> bool` Takes an object, a set and a *less-than* and determines if the object is in the set
- `oset-min : <oset> -> obj` Returns the least element in the set
- `oset=? : <oset> x <oset> x proc x proc -> bool` Takes two sets, a *less-than* and a *same?* procedure and checks if both sets are the same

## set collection operations
- `oset-add : <oset> x obj x proc -> <oset>` Takes a set, an object and a *less-than* and returns a set like the given one but with the object as a member
- `oset-delete : <oset> x obj x proc -> <oset>` Takes a set, an object and a *less-than* and returns a set like the given one but whitout the object as a member 
- `oset-delete-min : <oset> x proc -> <oset>` Takes a set and a *less-than* and returns a set like the given one but without the object as a member

## set operations
- `oset-union : <oset> x <oset> x proc -> <oset>`
- `oset-difference : <oset> x <oset> x proc -> <oset>`
- `oset-intersection : <oset> x <oset> x proc -> <oset>`

## set processing
- `oset-fold : proc x obj x <oset> -> <obj>` Inorder traversal where the given procedure is binary (e x es) where es is the rest of the tree and e < y for all y in es
- `oset-filter : proc x <oset> x proc -> <oset>` Takes a predicate, a set and a *less-than* and filters the elements of the set that satisfy the predicate
- `oset-map : proc x <oset> x proc -> <oset>` Takes a procedure, a set and a *less-than* and applies the procedure with each element of the set to produce another set

## set conversion
- `oset->list : <oset> x proc -> list` Takes a set and a *less-than* and returns an ordered list with the elements of the set
- `list->oset : list x proc -> <oset>` Takes a list and a *less-than* and returns a set with the elements of the list
